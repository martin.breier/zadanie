<?php


namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\BranchModelRepository;

class BranchModelController extends AbstractController
{
    /**
     * @var BranchModelRepository
     */
    private $branchModelRepository;

    public function __construct(BranchModelRepository $branchModelRepository)
    {
        $this->branchModelRepository = $branchModelRepository;
    }

    /**
     * @Route("branch/", name="get_branch", methods={"get"})
     */
    public function getAll(): JsonResponse
    {
        $branchModels = $this->branchModelRepository->findAll();
        $branchModelsArray = [];
        foreach ($branchModels as $key => $branchModel) {
            $branchModelArray[$key]['id'] = $branchModel->getId();
            $branchModelArray[$key]['internalId']=$branchModel->getInternalId();
            $branchModelArray[$key]['internalName']= $branchModel->getInternalName();
            $branchModelArray[$key]['location'] = ['latitude'=>$branchModel->getLocation()[0],'longitude'=>$branchModel->getLocation()[1]];
            $bussinesHourModelArray = [];
            foreach($branchModel->getBussinesHourModels() as $bussinesHourModel){
                $bussinesHourModelArray[] = ['dayOfWeek'=>$bussinesHourModel->getDayOfWeek(),'businessHour'=>$bussinesHourModel->getBusinessHour()];
            }
            $branchModelArray[$key]['bussinesHourModels']= $bussinesHourModelArray;
        }

        return new JsonResponse($branchModelArray, Response::HTTP_OK);
    }

    /**
     * @Route("/branch/{id}", name="get_one_branch", methods={"GET"})
     */
    public function getOne($id): JsonResponse
    {
        $branchModel = $this->branchModelRepository->find($id);
        $branchModelsArray = [];

            $branchModelArray['id'] = $branchModel->getId();
            $branchModelArray['internalId']=$branchModel->getInternalId();
            $branchModelArray['internalName']= $branchModel->getInternalName();
            $branchModelArray['location'] = ['latitude'=>$branchModel->getLocation()[0],'longitude'=>$branchModel->getLocation()[1]];
            $bussinesHourModelArray = [];
            foreach($branchModel->getBussinesHourModels() as $bussinesHourModel){
                $bussinesHourModelArray[] = ['dayOfWeek'=>$bussinesHourModel->getDayOfWeek(),'businessHour'=>$bussinesHourModel->getBusinessHour()];
            }
            $branchModelArray['bussinesHourModels']= $bussinesHourModelArray;


        return new JsonResponse($branchModelArray, Response::HTTP_OK);
    }
}
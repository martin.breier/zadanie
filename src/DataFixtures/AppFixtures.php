<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\BranchModel;
use App\Entity\BusinessHourModel;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $branchModel = new BranchModel();
        $branchModel->setLocation(['1111111', '222222222']);
        $branchModel->setInternalName('1234567');
        $branchModel->setInternalId('1234567');
        $branchModel->addBussinesHourModel('monday','16:00');
        $branchModel->addBussinesHourModel('tuesday','16:00');
        $branchModel->addBussinesHourModel('wednesday','16:00');
        $branchModel->addBussinesHourModel('thursday','16:00');
        $branchModel->addBussinesHourModel('friday','16:00');
        $manager->persist($branchModel);
        $manager->flush();
    }
}

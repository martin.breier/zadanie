<?php


namespace App\Entity;

use App\Repository\BranchModelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BusinessHourModel;

/**
 * @ORM\Entity(repositoryClass=BranchModelRepository::class)
 */
class BranchModel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $internalId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $internalName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $location;


    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="BusinessHourModel", mappedBy="branchModel",cascade={"persist", "remove"})
     */
    private $bussinesHourModels;



    public function __construct()
    {
        $this->bussinesHourModels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInternalId(): ?string
    {
        return $this->internalId;
    }

    public function setInternalId(string $internalId): void
    {
        $this->internalId = $internalId;
    }

    public function getInternalName(): ?string
    {
        return $this->internalName;
    }

    public function setInternalName(string $internalName): void
    {
        $this->internalName = $internalName;
    }

    public function getLocation(): ?array
    {
        return explode(',',$this->location);
    }

    public function setLocation(array $location): void
    {
        $this->location = implode(',',$location);
    }

    /**
     * @return Collection|BussinesHourModel[]
     */
    public function getBussinesHourModels(): Collection
    {
        return $this->bussinesHourModels;
    }

    public function addBussinesHourModel(string $dayOfWeek, string $businessHour): void
    {
        $bussinesHourModels = new BusinessHourModel($this,$dayOfWeek,$businessHour);
        $this->bussinesHourModels->add($bussinesHourModels);

    }




}
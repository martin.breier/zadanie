<?php


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\BusinessHourModelRepository;
use App\Entity\BranchModel;

/**
 * @ORM\Entity(repositoryClass=BusinessHourModelRepository::class)
 */
class BusinessHourModel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dayOfWeek;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $businessHour;


    /**
     * @ORM\ManyToOne(targetEntity="BranchModel", inversedBy="bussinesHourModels")
     * @ORM\JoinColumn(name="branch_model_id", referencedColumnName="id")
     */
    private $branchModel;

    public function __construct(BranchModel $branchModel, string $dayOfWeek, string $businessHour)
    {
        $this->branchModel = $branchModel;
        $this->dayOfWeek = $dayOfWeek;
        $this->businessHour = $businessHour;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDayOfWeek(): ?string
    {
        return $this->dayOfWeek;
    }

    public function setDayOfWeek(string $dayOfWeek): self
    {
        $this->dayOfWeek = $dayOfWeek;

        return $this;
    }

    public function getBusinessHour(): ?string
    {
        return $this->businessHour;
    }

    public function setBusinessHour(string $businessHour): self
    {
        $this->businessHour = $businessHour;

        return $this;
    }

    public function getBranchModel(): ?branchModel
    {
        return $this->branchModel;
    }

    public function setBranchModel(?branchModel $branchModel): self
    {
        $this->branchModel = $branchModel;

        return $this;
    }
}
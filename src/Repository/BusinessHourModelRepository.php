<?php

namespace App\Repository;

use App\Entity\BusinessHourModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BusinessHourModel|null find($id, $lockMode = null, $lockVersion = null)
 * @method BusinessHourModel|null findOneBy(array $criteria, array $orderBy = null)
 * @method BusinessHourModel[]    findAll()
 * @method BusinessHourModel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BusinessHourModelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BusinessHourModel::class);
    }

    // /**
    //  * @return BusinessHourModel[] Returns an array of BusinessHourModel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BusinessHourModel
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
